# Continuous Spectrum Estimation Algorithm - Experiments

This repository contains the results of numerous experiments evaluating [CSEA](https://bitbucket.org/maik-schmidt/csea). All experiments can be conveniently navigated via a graphical user interface. The repository can also be used to evaluate user-defined configurations.

## View Results
If you want to examine the experiments conducted as part of my [Master's thesis](https://www-ai.cs.tu-dortmund.de/auto?self=%24Publication_fw1n66po1s), simply clone this repository, install the required python packages and open the GUI.
```
git clone git@bitbucket.org/maik-schmidt/csea-exp.git
cd csea-exp
pip3 install -r requirements.txt
cd experiments
python3 view_results_gui.py
```

## Evaluate User-defined Configurations
If you want to do your own experiments, you must also install the csea python package
```
git clone git@bitbucket.org/maik-schmidt/csea.git
cd csea
pip install -e .
```

Comment in the desired configurations in _experiments/experiment_runner.py_. Note that the IACT data sets will not be available as they are not included in this repository. The results will be saved to a new folder, so make sure to update the variable _result_folder_ in _experiments/view_results_gui.py_. Run the experiments and open the GUI.
```
cd experiments
python3 experiment_runner.py
python3 view_results_gui.py
```

![GUI](docs/gui.png)
