import json
import os
import pickle
from pathlib import Path

import csea.calibration as calibration
import csea.stepsize as stepsize
import h5py
import numpy as np
from csea import csea, dsea
from csea.discretize import TreeDiscretizer
from sklearn.model_selection import RepeatedKFold
from sklearn.model_selection import ShuffleSplit
from sklearn.preprocessing import StandardScaler


class Experiment:
    """Run Experiments

    # Minimal example:
    # from sklearn.ensemble import RandomForestRegressor
    # from csea.Regressor import CompositeRegressor
    # from csea.DensityEstimator import Histogram
    # from Experiment import Experiment
    #
    # experiment = Experiment()
    # experiment.load_data('toy_data')
    # experiment.run(None, CompositeRegressor(RandomForestRegressor()), Histogram())
    # experiment.write_to_disk()
    """

    def __init__(self, alpha='adaptive', inspect='write_to_dict', calibrator=None, K=10, n_bins=20, n_folds=20,
                 seed=None):
        self.inspect = self.write_to_dict if inspect == 'write_to_dict' else inspect
        self.alpha = alpha
        self.K = K
        self.n_bins = n_bins
        self.n_folds = n_folds
        self.seed = seed
        self.bins = None
        self.calibrator = calibrator  # moments, quantiles or quantilesbeta

        self.X = None
        self.y = None
        self.results = {
            '1_K': K,
            '1_n_bins': n_bins,
            '1_n_folds': n_folds,
            '1_calibration': calibrator,
            '3_y_train': [],
            '3_y_data': [],
            '3_y_pred': [],
            '3_csea_contributions': [],
            '3_dsea_contributions': [],
            '3_f_dsea': [],
            '3_f_csea': [],
            '2_k_dsea': [],
            '2_k_csea': [],
            '2_alphak_dsea': [],
            '2_alphak_csea': [],
            '2_chi2s_dsea': [],
            '2_chi2s_csea': []
        }

    def load_data(self, data_set='toy_data', max_size=150000, filter_data=False):
        min_allowed, max_allowed = -np.inf, np.inf
        data_dir = os.path.join(Path(__file__).parent, '..', 'data', data_set)
        if data_set == 'toy_data':
            file = h5py.File(os.path.join(data_dir, 'gaussian.hdf5'), 'r')
            self.X = np.column_stack((file['data/x1'], file['data/x2'], file['data/x3'], file['data/x4'],
                                      file['data/x5'], file['data/x6'], file['data/x7'], file['data/x8'],
                                      file['data/x9'], file['data/x10']))[:max_size]
            self.y = np.array(file['data/y'])[:max_size]
            min_allowed, max_allowed = 8.0, 39.3

        elif data_set == 'fact':
            file = os.path.join(data_dir, 'fact_deconvolution.csv')
            data = np.genfromtxt(file, delimiter=',', max_rows=max_size, skip_header=True)
            min_allowed, max_allowed = 2.4, 4.2

        elif data_set == 'magic':
            file = os.path.join(data_dir, 'magic.csv')
            data = np.genfromtxt(file, delimiter=',', max_rows=max_size, skip_header=True)
            data[:, 0], data[:, 6] = data[:, 6], data[:, 0].copy()
            min_allowed, max_allowed = 1.6, 4.4

        else:
            data = np.genfromtxt(data_set, delimiter=',', max_rows=max_size, skip_header=True)

        if data_set != 'toy_data':
            self.X = data[:, 1:]
            self.y = data[:, 0]
        if filter_data:
            self.X, self.y = self.filter_data(self.X, self.y, min_allowed=min_allowed, max_allowed=max_allowed)

        self.bins = np.linspace(np.min(self.y), np.max(self.y), self.n_bins + 1)
        self.results['1_bins'] = self.bins
        self.results['1_data_set'] = data_set
        self.results['1_n_samples'] = len(self.y)

    def run(self, dsea_classifier, csea_regressor, csea_density_estimator, return_results=False):
        print('Running experiment with {}, {} and {}'.format(dsea_classifier.__class__.__name__,
                                                             csea_regressor.__class__.__name__,
                                                             csea_density_estimator.__class__.__name__))
        print('Repeated {}-fold cross-validation of {} deconvolution iterations'.format(self.n_folds, self.K))

        self.results['1_dsea_classifier'] = self.to_str_dict(dsea_classifier)
        self.results['1_csea_regressor'] = self.to_str_dict(csea_regressor)
        self.results['1_csea_density_estimator'] = self.to_str_dict(csea_density_estimator)
        f_dsea, f_csea, contributions_dsea, contributions_csea = [], [], [], []

        if self.n_folds > 1:
            split = RepeatedKFold(n_splits=3, n_repeats=(self.n_folds + 1) // 3, random_state=self.seed).split(self.X,
                                                                                                               self.y)
        else:
            split = ShuffleSplit(1, train_size=1 / 3, random_state=self.seed).split(self.X, self.y)

        for fold, (train_idx, data_idx) in zip(range(self.n_folds), split):  # Limits iter to n_folds
            print('Fold {} of {}'.format(fold + 1, self.n_folds))

            X_train, y_train = self.X[train_idx], self.y[train_idx]
            X_data, y_data = self.X[data_idx], self.y[data_idx]
            X_train, X_data, y_train, y_data = self.scale(X_train, X_data, y_train, y_data)
            y_train_discrete, bins, alpha = self.discretize(X_train, X_data, y_train, self.bins)
            calibrator = self.calibrate(X_train, y_train, X_data, csea_regressor, self.calibrator)

            if hasattr(csea_density_estimator, 'bins'):
                csea_density_estimator.bins = bins

            if dsea_classifier is not None:
                result_dsea = dsea(X_data, X_train, y_train_discrete, dsea_classifier, bins=np.arange(self.n_bins),
                                   K=self.K, inspect=self.inspect, alpha=alpha, return_contributions=True)
                f_dsea.append(result_dsea[0])
                contributions_dsea.append(result_dsea[1])
                self.results['3_dsea_contributions'].append(result_dsea[1])

            if csea_regressor is not None and csea_density_estimator is not None:
                result_csea = csea(X_data, X_train, y_train, csea_regressor, csea_density_estimator, bins=bins,
                                   K=self.K, inspect=self.inspect, alpha=alpha, return_contributions=True,
                                   calibrator=calibrator)
                f_csea.append(result_csea[0])
                contributions_csea.append(result_csea[1])
                self.results['3_csea_contributions'].append(result_csea[1])

            self.results['3_y_train'].append(y_train)
            self.results['3_y_data'].append(y_data)

        if return_results:
            return f_dsea, f_csea, contributions_dsea, contributions_csea

    def write_to_disk(self, file_name='results', write_json=False):
        class NumpyEncoder(json.JSONEncoder):
            def default(self, obj):
                if isinstance(obj, np.ndarray):
                    return obj.tolist()
                return json.JSONEncoder.default(self, obj)

        os.makedirs(os.path.dirname(file_name), exist_ok=True)

        if write_json:
            f = open(file_name + '.json', 'w')
            f.write(json.dumps(self.results, cls=NumpyEncoder, indent=4, separators=(',', ': '), sort_keys=True))
            f.close()

        f = open(file_name + '.pkl', 'wb')
        pickle.dump(self.results, f)
        f.close()

        print('Results written to disk: {}'.format(file_name + '.pkl'))

    def scale(self, X_train, X_data, y_train, y_data, scale_X=True, scale_y=False):
        scaler = StandardScaler()
        if scale_X:
            X_train = scaler.fit_transform(X_train)
            X_data = scaler.transform(X_data)
        if scale_y:
            y_train = scaler.fit_transform(y_train[:, np.newaxis])[:, 0]
            y_data = scaler.transform(y_data[:, np.newaxis])[:, 0]
        return X_train, X_data, y_train, y_data

    def discretize(self, X_train, X_data, y_train, bins=None):
        if bins is None:
            bins = np.linspace(np.min(y_train), np.max(y_train), self.n_bins + 1)
        y_train_discrete = np.digitize(y_train, bins[1:-1])

        alpha = self.alpha
        if alpha == 'adaptive':
            discretizer = TreeDiscretizer(X_train, y_train_discrete, self.n_bins, seed=self.seed)
            alpha = stepsize.alpha_adaptive_run(discretizer.discretize(X_data), discretizer.discretize(X_train),
                                                y_train_discrete)
        return y_train_discrete, bins, alpha

    def calibrate(self, X_train, y_train, X_data, regressor, calibrator=None):
        if calibrator == 'moments':
            calibrator = calibration.get_moment_calibrator(X_train, y_train, regressor)
        elif calibrator == 'quantiles':
            calibrator = calibration.get_quantile_calibrator(X_train, y_train, regressor, calibrator='gp',
                                                             n_quantiles=len(X_data))
        elif calibrator == 'quantilesbeta':
            calibrator = calibration.get_quantile_calibrator(X_train, y_train, regressor, calibrator='beta',
                                                             n_quantiles=len(X_data))
        else:
            pass
        return calibrator

    def write_to_dict(self, f, k, alphak, chi2s):
        method = 'dsea' if len(f) == self.n_bins else 'csea'
        if k == 0:
            self.results['3_f_' + method].append([f])
            self.results['2_k_' + method].append([k])
            self.results['2_alphak_' + method].append([alphak])
            self.results['2_chi2s_' + method].append([chi2s])

        else:
            self.results['3_f_' + method][-1].append(f)
            self.results['2_k_' + method][-1].append(k)
            self.results['2_alphak_' + method][-1].append(alphak)
            self.results['2_chi2s_' + method][-1].append(chi2s)

        # print('f: {}..., k: {}, alphak: {}, chi2s: {}'.format(f[:10], k, alphak, chi2s))

    def filter_data(self, X, y, min_allowed, max_allowed):
        valid = np.logical_and(y >= min_allowed, y <= max_allowed)
        X = X[valid]
        y = y[valid]
        return X, y

    def to_str_dict(self, cls):
        if cls is None:
            return
        d = dict((key, str(value)) for key, value in cls.__dict__.items())
        d['__name__'] = cls.__class__.__name__
        return d
