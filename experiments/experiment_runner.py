import multiprocessing
import os
import traceback

from Experiment import Experiment
from csea.DensityEstimator import Histogram, KernelDensityEstimator
from csea.Regressor import GaussianProcessRegressor, CompositeRegressor, MondrianForestRegressor, MondrianTreeRegressor, KernelRidgeRegressor
from csea.stepsize import decay_exp, decay_mul
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from copy import deepcopy
from sklearn.base import clone

K = 5
n_folds = 3
n_bins = 20
max_size = 10000
out_dir = 'results'
n_jobs = multiprocessing.cpu_count()
calibrators = [
    None,
    #'moments',
    #'quantiles',
    #'quantilesbeta',
]
data_sets = [
    'toy_data',
    #'fact',
    #'magic'
]
classifier = None#RandomForestClassifier(n_estimators=30, n_jobs=n_jobs)
regressors = [
    GaussianProcessRegressor(),
    KernelRidgeRegressor(kernel_density_estimator='gaussian'),
    #MondrianTreeRegressor(),
    #MondrianForestRegressor(n_estimators=30, n_jobs=n_jobs),
    CompositeRegressor(RandomForestRegressor(n_estimators=30, n_jobs=n_jobs), None)
]
density_estimators = [
    Histogram(),
    #Histogram(weighted=True),
    KernelDensityEstimator(kernel='epanechnikov', silverman_factor=0.5),
    KernelDensityEstimator(kernel='epanechnikov', silverman_factor=0.9),
    KernelDensityEstimator(kernel='epanechnikov', silverman_factor=1.06),
    #KernelDensityEstimator(kernel='epanechnikov', silverman_factor=0.5/2, bandwidth='uncertainty_add'),
    #KernelDensityEstimator(kernel='epanechnikov', silverman_factor=0.5/2, bandwidth='uncertainty_mult'),
    #KernelDensityEstimator(weighted=True, kernel='epanechnikov'),
]
alpha = [
    #1,
    'adaptive',
    #decay_mul(eta=0.5),
]

abbreviations = {
    'RandomForestRegressor': 'rf',
    'MondrianForestRegressor': 'mf',
    'MondrianTreeRegressor': 'mt',
    'GaussianProcessRegressor': 'gp',
    'KernelRidgeRegressor': 'krr',
    'CompositeRegressor': 'comp',
    'Histogram': 'hist',
    'KernelDensityEstimator': 'kde',
    'silverman': 'rot',
    'uncertainty_add': 'uadd',
    'uncertainty_mult': 'umul'
}


def to_file_name(regressor, density_estimator, alpha, calibration):
    file_name = abbreviations.get(regressor.__class__.__name__, 'NA') + '_'
    if file_name.startswith(abbreviations.get('CompositeRegressor')):
        file_name += abbreviations.get(regressor.model.__class__.__name__, 'NA') + '_'
        file_name += abbreviations.get(regressor.uncertainty_model.__class__.__name__, 'NA') + '_'
    if hasattr(density_estimator, 'weighted') and density_estimator.weighted:
        file_name += 'w'
    file_name += abbreviations.get(density_estimator.__class__.__name__, 'NA') + '_'
    if hasattr(density_estimator, 'method') and density_estimator.method is not None:
        file_name += abbreviations.get(density_estimator.method) + '_'
    if hasattr(density_estimator, 'silverman_factor'):
        file_name += str(density_estimator.silverman_factor) + '_'
    file_name += 'adapt' if alpha == 'adaptive' else 'decay' if callable(alpha) else str(alpha)
    if calibration is not None:
        file_name += '_cal' + calibration[:1]
        if calibration[-4:] == 'beta':
            file_name += 'b'
    return file_name


for s in data_sets:
    for r in regressors:
        for d in density_estimators:
            for a in alpha:
                for c in calibrators:
                    try:
                        experiment = Experiment(n_bins=n_bins, n_folds=n_folds, K=K, alpha=a, calibrator=c)
                        experiment.load_data(s, max_size=max_size, filter_data=s == 'toy_data')
                        experiment.run(None if classifier is None else clone(classifier), deepcopy(r), deepcopy(d))
                        experiment.write_to_disk(os.path.join(out_dir, s, to_file_name(r, d, a, c)))
                    except:
                        print(traceback.format_exc())
