import os
import pickle
import tkinter as tk

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.ticker import MaxNLocator
from scipy.stats import norm
import seaborn as sns
sns.set()

#exec(open(os.path.join('scripts', 'latex_preamble.py')).read())

result_folder = 'results_thesis'
results = [None, None]
n_approx = 1000
epsilon = 1e-2
legend_size = None
label_fontsize = None


def normal(x, mu1=15, mu2=30, s1=3, s2=4, wei2=0.5):
    return (1 - wei2) * norm.pdf(x, mu1, s1) + wei2 * norm.pdf(x, mu2, s2)


def kolmogorov(x1, f1, x2, F2):  # Must be sorted
    x1_left_of_x2 = np.searchsorted(x1[1:], x2)  # Remove first element to get the next smaller element
    x2_left_of_x1 = np.searchsorted(x2[1:], x1)

    F1 = np.cumsum(f1) / np.sum(f1)

    max_distance11 = np.max(np.abs(F1 - F2[x2_left_of_x1]))
    max_distance12 = np.max(np.abs(np.insert(F1[:-1], 0, 0) - F2[x2_left_of_x1]))
    max_distance22 = np.max(np.abs(F2 - F1[x1_left_of_x2]))
    max_distance21 = np.max(np.abs(np.insert(F2[:-1], 0, 0) - F1[x1_left_of_x2]))

    return max([max_distance11, max_distance12, max_distance21, max_distance22])


def earth_mover(x1, f1, x2, F2):  # Must be sorted
    F1 = np.cumsum(f1) / np.sum(f1)

    approx = np.linspace(0, 1, n_approx)
    quantile_indices_1 = np.searchsorted(F1[1:], approx)  # Remove first element to get the next smaller element
    quantile_indices_2 = np.searchsorted(F2[1:], approx)
    return np.sum(np.abs(x1[quantile_indices_1] - x2[quantile_indices_2])) / n_approx


def dict_values_to_np(values, i, fold):
    return tuple(np.array(results[i][v][fold]) for v in values)


def approx_csea(f_csea, y_train, bin_representatives, approx):
    sort_idx = np.argsort(y_train)
    y_train_sorted = y_train[sort_idx][1:]  # Remove first element to get the next smaller element with searchsorted
    f_csea_bin_idx = np.searchsorted(y_train_sorted, bin_representatives)
    f_csea_approx_idx = np.searchsorted(y_train_sorted, approx)
    return f_csea[:, sort_idx[f_csea_bin_idx]], f_csea[:, sort_idx[f_csea_approx_idx]]


def sum_min_max(a, b_sum=0, b_min=-np.inf, b_max=np.inf):
    return np.add(a, b_sum), np.minimum(a, b_min), np.maximum(a, b_max)


def add_mean_min_max_to_result(i):
    global results
    y_data = np.concatenate([dict_values_to_np(['3_y_data'], i, f)[0] for f in range(results[i]['1_n_folds'])])
    y_data_sorted = np.sort(y_data)
    bins = results[i]['1_bins']
    bin_representatives = [(bins[i] + bins[i + 1]) / 2 for i in range(len(bins) - 1)]
    approx = np.linspace(bins[0], bins[-1], n_approx)
    keys = ['3_f_csea', '3_y_train', '3_y_data', '3_csea_contributions']
    include_dsea = results[i]['1_dsea_classifier'] is not None

    K = results[i]['1_K'] + 1
    f_dsea_mean = np.zeros((K, len(bins) - 1))
    f_dsea_lower = np.full((K, len(bins) - 1), np.inf)
    f_dsea_upper = np.zeros((K, len(bins) - 1))
    f_csea_bin_mean = np.zeros((K, len(bins) - 1))
    f_csea_bin_lower = np.full((K, len(bins) - 1), np.inf)
    f_csea_bin_upper = np.zeros((K, len(bins) - 1))
    f_csea_approx_mean = np.zeros((K, n_approx))
    f_csea_approx_lower = np.full((K, n_approx), np.inf)
    f_csea_approx_upper = np.zeros((K, n_approx))

    n_folds = results[i]['1_n_folds']
    for fold in range(n_folds):
        f_csea, y_train, y_data_fold, csea_contributions = dict_values_to_np(keys, i, fold)

        f_csea_bin, f_csea_approx = approx_csea(f_csea, y_train, bin_representatives, approx)
        f_csea_bin_mean, f_csea_bin_lower, f_csea_bin_upper = sum_min_max(f_csea_bin, f_csea_bin_mean, f_csea_bin_lower,
                                                                          f_csea_bin_upper)
        f_csea_approx_mean, f_csea_approx_lower, f_csea_approx_upper = sum_min_max(f_csea_approx, f_csea_approx_mean,
                                                                                   f_csea_approx_lower,
                                                                                   f_csea_approx_upper)

        if include_dsea:
            f_dsea = dict_values_to_np(['3_f_dsea'], i, fold)[0]
            f_dsea_mean, f_dsea_lower, f_dsea_upper = sum_min_max(f_dsea, f_dsea_mean, f_dsea_lower, f_dsea_upper)

    f_csea_bin_mean = f_csea_bin_mean / n_folds
    f_csea_approx_mean = f_csea_approx_mean / n_folds
    results[i]['2_f_csea_bin_mean'] = f_csea_bin_mean
    results[i]['2_f_csea_bin_lower'] = f_csea_bin_lower
    results[i]['2_f_csea_bin_upper'] = f_csea_bin_upper
    results[i]['2_f_csea_approx_mean'] = f_csea_approx_mean
    results[i]['2_f_csea_approx_lower'] = f_csea_approx_lower
    results[i]['2_f_csea_approx_upper'] = f_csea_approx_upper
    results[i]['y_data_sorted'] = y_data_sorted

    if include_dsea:
        f_dsea_mean = f_dsea_mean / n_folds
        results[i]['2_f_dsea_mean'] = f_dsea_mean
        results[i]['2_f_dsea_lower'] = f_dsea_lower
        results[i]['2_f_dsea_upper'] = f_dsea_upper

    # f = open(os.path.join(result_folder, var[i].get()), 'wb')
    # pickle.dump(results[i], f)
    # f.close()


def change_data_set(event=None, i=0):
    pickle_file = open(os.path.join(result_folder, var[i].get()), 'rb')
    global results
    results[i] = pickle.load(pickle_file)
    pickle_file.close()

    print('Loaded result from {}:'.format(var[i].get()))
    for k, v in results[i].items():
        if k.startswith('1'):
            print(k, v)

    global scale_k
    scale_k[i] = tk.Scale(options_right if i else options_left, from_=0, to=results[i]['1_K'],
                          orient=tk.HORIZONTAL, command=update_right_plot if i else update_plot)
    scale_k[i].grid(column=2, row=2)

    if '2_f_csea_mean' not in results[i]:
        add_mean_min_max_to_result(i)
    update_plot(None, i)


def change_right_data_set(event=None):
    change_data_set(event, 1)


def update_plot(event=None, i=0):
    if var[i].get().startswith('Choose'):
        return
    ax = axes[i]
    k = scale_k[i].get()
    is_toy_data_set = results[i]['1_data_set'] == 'toy_data'
    bins = results[i]['1_bins']
    bin_representatives = [(bins[i] + bins[i + 1]) / 2 for i in range(len(bins) - 1)]
    approx = np.linspace(bins[0], bins[-1], n_approx)
    include_dsea = results[i]['1_dsea_classifier'] is not None

    f_csea_bin_mean = results[i]['2_f_csea_bin_mean'][k]
    f_csea_approx_mean = results[i]['2_f_csea_approx_mean'][k]
    f_csea_bin_lower = results[i]['2_f_csea_bin_lower'][k]
    f_csea_approx_lower = results[i]['2_f_csea_approx_lower'][k]
    f_csea_bin_upper = results[i]['2_f_csea_bin_upper'][k]
    f_csea_approx_upper = results[i]['2_f_csea_approx_upper'][k]
    csea_contributions_0 = dict_values_to_np(['3_csea_contributions'], i, 0)[0]
    y_data_sorted = results[i]['y_data_sorted']

    if include_dsea:
        f_dsea_mean = results[i]['2_f_dsea_mean'][k]
        f_dsea_lower = results[i]['2_f_dsea_lower'][k]
        f_dsea_upper = results[i]['2_f_dsea_upper'][k]

    ax.clear()
    plot_option = var[2].get()
    if plot_option in plot_options[:2]:  # Probability
        ax.plot([], [], '+k', label=r'y (predict) (subset)')  # Hack to add predictions to legend

        if plot_option == plot_options[0]:  # PDF
            # CSEA
            ax.plot(approx, f_csea_approx_mean, label=r'CSEA', color='k')
            ax.errorbar(bin_representatives, f_csea_bin_mean,
                        # + ((bins[1] - bins[0]) / 8) if include_dsea else bin_representatives, f_csea_bin_mean,
                        yerr=np.abs(f_csea_bin_mean - np.stack((f_csea_bin_lower, f_csea_bin_upper))), fmt='none',
                        capsize=4, alpha=0.5, color='k')
            # DSEA
            if include_dsea:
                ax.plot(bins, [0] + list(f_dsea_mean * (len(bins) - 1) / (bins[-1] - bins[0])),
                        drawstyle='steps', label=r'DSEA', color='b', linestyle='--')
                eb = ax.errorbar(bin_representatives,  # - ((bins[1] - bins[0]) / 8),
                                 f_dsea_mean * (len(bins) - 1) / (bins[-1] - bins[0]),
                                 yerr=np.abs(f_dsea_mean - np.stack((f_dsea_lower, f_dsea_upper))), fmt='none',
                                 capsize=4,
                                 alpha=0.5, color='b')
                eb[-1][0].set_linestyle('--')
            if is_toy_data_set:
                x = np.linspace(8.0, 39.3, n_approx)
                ax.fill_between(x, normal(x), alpha=0.5, color='y', label=r'True distribution')
            else:
                ax.hist(y_data_sorted, bins=bins, density=True, histtype='stepfilled', alpha=0.5, color='y',
                        label=r'y (observed)')
            axes[0].set_ylabel(r'P(y)', fontsize=label_fontsize)
            ax.legend(loc='upper right', fontsize=legend_size)

        elif plot_option == plot_options[1]:  # CDF
            # CSEA
            if results[i]['1_csea_density_estimator']['__name__'] == 'Histogram':
                ax.plot([bins[0]] + list(bin_representatives) + [bins[-1]],
                        [0] + list(np.cumsum(f_csea_bin_mean) / np.sum(f_csea_bin_mean)) + [1], drawstyle='steps-post',
                        label='CSEA', color='k')
            else:
                ax.plot(approx, np.cumsum(f_csea_approx_mean) / np.sum(f_csea_approx_mean), label=r'CSEA', color='k')
            # DSEA
            if include_dsea:
                ax.plot([bins[0]] + list(bin_representatives) + [bins[-1]],
                        [0] + list(np.cumsum(f_dsea_mean) / np.sum(f_dsea_mean)) + [1], drawstyle='steps-post',
                        label=r'DSEA', color='b', linestyle='--')
            y_data_idx = np.linspace(0, len(y_data_sorted) - 1, n_approx, dtype=int)
            ax.fill_between(list(y_data_sorted[y_data_idx]), np.arange(n_approx) / n_approx,
                            step='post', alpha=0.5, label=r'y (observed)', color='y')
            axes[0].set_ylabel(r'Cumulative probability', fontsize=label_fontsize)
            ax.legend(loc='upper left', fontsize=legend_size)

        ax.set_ylim(bottom=0)
        ax.set_xlim(bins[0], bins[-1])
        ax.set_xlabel(r'y', fontsize=label_fontsize)

        ax_twin = twinx[i]
        ax_twin.clear()
        y_pred = [c[0] for c in csea_contributions_0 if bins[0] <= c[0] <= bins[-1]]
        ax_twin.plot(y_pred[:100], 0.02 + 0.01 * np.random.random(len(y_pred[:100])), '+k')
        ax_twin.set_ylim(0, 1)
        ax_twin.set_yticks([])

    else:  # Loss
        twinx[i].clear()
        twinx[i].set_yticks([])
        K = results[i]['1_K']
        n_folds = results[i]['1_n_folds']
        if include_dsea:
            chi2s_dsea = np.stack([dict_values_to_np(['2_chi2s_dsea'], i, f)[0] for f in range(n_folds)])
            chi2s_dsea_mean = np.mean(chi2s_dsea, axis=0)
            dsea_stopped_k = next((i for i, c in enumerate(chi2s_dsea_mean) if c < epsilon), K)

        chi2s_csea = np.stack([dict_values_to_np(['2_chi2s_csea'], i, f)[0] for f in range(n_folds)])
        chi2s_csea_mean = np.mean(chi2s_csea, axis=0)
        csea_stopped_k = next((i for i, c in enumerate(chi2s_csea_mean) if c < epsilon), K)

        if plot_option == plot_options[2]:  # Chi2s
            if include_dsea:
                ax.plot(range(1, len(chi2s_dsea_mean)), chi2s_dsea_mean[1:], label=r'DSEA',
                        color='b', linestyle='--', marker='.')
            ax.plot(range(1, len(chi2s_csea_mean)), chi2s_csea_mean[1:], label=r'CSEA', color='k', marker='.')
            axes[0].set_ylabel(r'chi^2', fontsize=label_fontsize)
            ax.axhline(epsilon, color='r', label=r'epsilon', linestyle='-.')

        elif plot_option == plot_options[3]:  # Kolmogorov distance
            if include_dsea:
                kolmogorov_distances = [kolmogorov(bin_representatives, results[i]['2_f_dsea_mean'][k], y_data_sorted,
                                                   np.arange(1, len(y_data_sorted) + 1) / len(y_data_sorted)) for k in
                                        range(K + 1)]
                #print('Kolmogorov: {}'.format(kolmogorov_distances))
                ax.plot(range(K + 1), kolmogorov_distances, label='DSEA', color='b', linestyle='--', marker='.')
                if dsea_stopped_k < K:
                    ax.axvline(dsea_stopped_k + 0.5, color='b', label=r'DSEA stopping criterion', linestyle=':',
                               alpha=0.5)
            if results[i]['1_csea_density_estimator']['__name__'] == 'Histogram':
                kolmogorov_distances = [
                    kolmogorov(bin_representatives, results[i]['2_f_csea_bin_mean'][k], y_data_sorted,
                               np.arange(1, len(y_data_sorted) + 1) / len(y_data_sorted)) for k in range(K + 1)]
            else:
                kolmogorov_distances = [kolmogorov(approx, results[i]['2_f_csea_approx_mean'][k], y_data_sorted,
                                                   np.arange(1, len(y_data_sorted) + 1) / len(y_data_sorted)) for k in
                                        range(K + 1)]
            #print('Kolmogorov: {}'.format(kolmogorov_distances))
            ax.plot(range(K + 1), kolmogorov_distances, label='CSEA', color='k', marker='.')
            axes[0].set_ylabel(r'Kolmogorov distance', fontsize=label_fontsize)
            if csea_stopped_k < K:
                ax.axvline(csea_stopped_k + 0.5, color='k', label=r'CSEA stopping criterion', linestyle='-.', alpha=0.5)
            ax.set_ylim(bottom=0)

        elif plot_option == plot_options[4]:  # Earth mover's distance
            earth_movers_distances = [earth_mover(approx, results[i]['2_f_csea_approx_mean'][k], y_data_sorted,
                                                  np.arange(1, len(y_data_sorted) + 1) / len(y_data_sorted)) for k in
                                      range(K + 1)]
            #print('EMD: {}'.format(earth_movers_distances))
            ax.plot(range(K + 1), earth_movers_distances, label='CSEA', color='k', marker='.')
            axes[0].set_ylabel(r'Earth movers distance', fontsize=label_fontsize)
            if csea_stopped_k < K:
                ax.axvline(csea_stopped_k + 0.5, color='k', label=r'CSEA stopping criterion', linestyle='-.', alpha=0.5)
            ax.set_ylim(bottom=0)

        ax.set_xlim(0, K + 1)
        ax.set_xlabel(r'k', fontsize=label_fontsize)
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.legend(loc='upper right', fontsize=legend_size)

    canvas.draw()


def update_right_plot(event=None):
    update_plot(event, 1)


root = tk.Tk()
root.wm_title('Continuous Deconvolution')
fig, axes = plt.subplots(1, 2)
plt.rcParams.update({'lines.markeredgewidth': 1})
twinx = [axes[0].twinx(), axes[1].twinx()]
plt.tight_layout()

canvas = FigureCanvasTkAgg(fig, master=root)
canvas.draw()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

toolbar = NavigationToolbar2Tk(canvas, root)
toolbar.update()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

options_left, options_right = tk.Frame(master=root), tk.Frame(master=root)
options_left.pack(side=tk.LEFT)
options_right.pack(side=tk.RIGHT)
tk.Label(options_left, text='Data set').grid(column=1, row=1)
tk.Label(options_right, text='Data set').grid(column=1, row=1)
tk.Label(options_left, text='K').grid(column=1, row=2)
tk.Label(options_right, text='K').grid(column=1, row=2)
var = [tk.StringVar(root), tk.StringVar(root), tk.StringVar(root)]
var[0].set('Choose an experiment')
var[1].set('Choose an experiment')
var[2].set('Probability density function')
experiments = [os.path.join(os.path.relpath(r, result_folder), f) for r, _, files in os.walk(result_folder) for f in
               files if f.endswith('.pkl')]
plot_options = ['Probability density function', 'Cumulative distribution function', 'Chi-Squared distance',
                'Kolmogorov distance', 'Earth movers distance']
tk.OptionMenu(root, var[2], *plot_options, command=lambda x: [update_plot(x, 0), update_plot(x, 1)]).pack()
option_data_set = [tk.OptionMenu(options_left, var[0], *experiments, command=change_data_set),
                   tk.OptionMenu(options_right, var[1], *experiments, command=change_right_data_set)]
option_data_set[0].grid(column=2, row=1)
option_data_set[1].grid(column=2, row=1)
scale_k = [None, None]

tk.mainloop()
